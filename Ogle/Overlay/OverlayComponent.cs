﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Ogle.HUD
{
    public class OverlayComponent
    {
        public int Height;
        public int Width;
        public Vector2 Position;

    }
}