using System;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using Ogle.Interfaces;

namespace Ogle
{
    public class FirstPersonCamera : ICamera
    {
        #region Fields/Properties
        private Vector3 position;
        public Vector3 Position
        {
            get { return position; }
            private set { position = value; }
        }
        
        private Vector3 rotation;
        public Vector3 Rotation
        {
            get { return rotation; }
            private set { rotation = value; }
        }

        private Quaternion orientation;
        public Quaternion Orientation
        {
            get { return orientation; }
            private set { orientation = value; }
        }

        private Matrix4 cameraMatrix;
        public Matrix4 CameraMatrix {
            get { return cameraMatrix; }
            private set { cameraMatrix = value; }
        }

        private Matrix4 viewMatrix;
        public Matrix4 ViewMatrix 
        {
            get { return ViewMatrix; }
            private set { viewMatrix = value; }
        }

        private Matrix4 projectionMatrix;
        public Matrix4 ProjectionMatrix 
        {
            get { return projectionMatrix; }
            private set { projectionMatrix = value; }
        }

        private int ModelViewMatrixLocation;
        private int ProjectionMatrixLocation;

        public float DrawDistance;
        #endregion

        public FirstPersonCamera()
        {
            Initalize(new Vector3(0f, 0f, 0f));
        }
        public FirstPersonCamera(float x, float y, float z)
        {
            Initalize(new Vector3(x, y, z));
        }
        public FirstPersonCamera(Vector3 position)
        {
            Initalize(position);
        }

        private void Initalize(Vector3 position)
        {
            Position = position;
            DrawDistance = 1000f;

            CameraMatrix = Matrix4.Identity;
            ProjectionMatrix = Matrix4.Identity;
            Orientation = Quaternion.Identity;

            ModelViewMatrixLocation = GL.GetUniformLocation(Game.ShaderProgram, "ViewMatrix");
            ProjectionMatrixLocation = GL.GetUniformLocation(Game.ShaderProgram, "ProjectionMatrix");

            if (ModelViewMatrixLocation == -1) throw new NotImplementedException();
            if (ProjectionMatrixLocation == -1) throw new NotImplementedException();

            ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(MathHelper.PiOver4, Game.AspectRatio, 0.001f, 100f);
        }

        public void Update()
        {
            if (Input.MouseDelta.X != 0 || Input.MouseDelta.Y != 0)
            {
                RotateX(Input.MouseDelta.Y / 1000f);
                RotateY(-Input.MouseDelta.X / 1000f);
            }

            Orientation =
                Quaternion.FromAxisAngle(Vector3.UnitY, Rotation.Y) *
                Quaternion.FromAxisAngle(Vector3.UnitX, Rotation.X);

            var forward = Vector3.Transform(Vector3.UnitZ, Orientation);
            ViewMatrix = Matrix4.LookAt(Position, Position + forward, Vector3.UnitY);
        }

        public void Resize(float AspectRatio)
        {
            ProjectionMatrix = Matrix4.CreatePerspectiveFieldOfView(
                MathHelper.PiOver4, AspectRatio, 0.1f, DrawDistance
            );

            SendShaderData();
        }

        public void RotateX(float x)
        {
            rotation.X += x;
            rotation.X = MathHelper.Clamp(Rotation.X, -1.57f, 1.57f);

            SendShaderData();
        }
        public void RotateY(float y)
        {
            rotation.Y += y;
            rotation.Y = ClampCircular(Rotation.Y, 0, MathHelper.TwoPi);

            SendShaderData();
        }

        public void TranslateLeftRight(float x)
        {
            Position += Vector3.Transform(Vector3.UnitX * x, Quaternion.FromAxisAngle(Vector3.UnitY, Rotation.Y));
            SendShaderData();
        }
        public void TranslateUpDown(float y)
        {
            Position += Vector3.Transform(Vector3.UnitY * y, Quaternion.FromAxisAngle(Vector3.UnitY, Rotation.Y));
            SendShaderData();
        }
        public void TranslateForwardsBackwards(float z)
        {
            Position += Vector3.Transform(Vector3.UnitZ * z, Quaternion.FromAxisAngle(Vector3.UnitY, Rotation.Y));
            SendShaderData();
        }

        public void TranslateYLocal(float y)
        {
            Position += Vector3.Transform(Vector3.UnitY * y, Orientation);
            SendShaderData();
        }
        public void TranslateZLocal(float z)
        {
            Position += Vector3.Transform(Vector3.UnitZ * z, Orientation);
            SendShaderData();
        }

        private void SendShaderData()
        {
            GL.UniformMatrix4((int)ModelViewMatrixLocation, false, ref viewMatrix);
            GL.UniformMatrix4((int)ProjectionMatrixLocation, false, ref projectionMatrix);
        }

        public static float ClampCircular(float n, float min, float max)
        {
            if (n >= max) n -= max;
            if (n < min) n += max;
            return n;
        }
    }
}