﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;

namespace Ogle.Interfaces
{
    public interface ICamera
    {
        Vector3 Position { get; }
        Vector3 Rotation { get; }
        Quaternion Orientation { get; }

        Matrix4 CameraMatrix { get; }
        Matrix4 ViewMatrix { get; }
        Matrix4 ProjectionMatrix { get; }

        void RotateX(float x);
        void RotateY(float y);

        void TranslateLeftRight(float x);
        void TranslateUpDown(float y);
        void TranslateForwardsBackwards(float z);

        void TranslateYLocal(float y);
        void TranslateZLocal(float z);

        void Resize(float AspectRatio);
        void Update();
    }
}
