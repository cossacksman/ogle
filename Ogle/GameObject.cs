using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;

using Ogle.Shaders;

namespace Ogle
{
	public class GameObject
	{
		public Vector3 Position { get; private set; }
		public Vector3 Rotation { get; private set; }
		public Vector3 Velocity { get; private set; }
		public Vector3 Proportion { get; private set; }

		public Mesh ObjectMesh { get; private set; }

		/* Buffer Handles
         * [0] VAO handle 
         * [1] Vertex handle
         * [2] Index handle
         * [3] Color handle
         * [4] Texture handle
         * [5] Normal handle
         */
		private uint[] BufferHandles { get; set; }

		public uint ModelMatrixLocation;
		public Matrix4 ModelMatrix = Matrix4.Identity;
        public uint NormalMatrixLocation;
        public Matrix4 NormalMatrix = Matrix4.Identity;

		public GameObject()
		{
			Initialize();
		}
		public GameObject(Mesh MeshObject)
		{
			ObjectMesh = MeshObject;
			Initialize();
		}
		/// <summary>
		/// Sets the default values of world properties and
		/// binds all of the buffer handles for the mesh.
		/// </summary>
		private void Initialize()
		{
			Position = Vector3.Zero;
			Rotation = Vector3.Zero;
			Velocity = Vector3.Zero;
			Proportion = Vector3.One;

			GL.GetUniformLocation(Game.ShaderProgram, "ModelMatrix");

			if (ObjectMesh != null)
			{
			    BufferHandles = BufferFactory.CreateBuffers(
				    ObjectMesh.VertexData,
				    ObjectMesh.IndexData,
				    ObjectMesh.ColorData,
				    ObjectMesh.TextureData,
				    ObjectMesh.Normaldata
			    );
			}

            NormalMatrix = Matrix4.Transpose(Matrix4.Invert(ModelMatrix));
		}
        
		/// <summary>
		/// If a mesh is not passed in the constructor, you
		/// may still add one to the GameObject by passing
		/// in the model file name, mesh object or the raw
		/// vertex data of all kinds.
		/// </summary>
		public void AddMesh(Mesh MeshObject)
		{
			ObjectMesh = MeshObject;
		}
		public void AddMesh(string FileName)
		{
			ObjectMesh = MeshFactory.CreateFromFile(FileName);
		}
		public void AddMesh(float[] VertexArray, uint[] IndexArray, float[] ColorArray = null, float[] TextureArray = null, uint[] NormalArray = null)
		{
			ObjectMesh = new Mesh(VertexArray, IndexArray, ColorArray, TextureArray, NormalArray);
		}

		/// <summary>
		/// Creates a modelmatrix using scale, rotation and 
		/// translation methods from the Matrix4 type.
		/// </summary>
		/// <returns></returns>
		public Matrix4 GetSetModelMatrix()
		{
			ModelMatrix = Matrix4.CreateScale(Proportion) *
				          Matrix4.CreateRotationX(Rotation.X) *
					      Matrix4.CreateRotationY(Rotation.Y) *
					      Matrix4.CreateRotationZ(Rotation.Z) *
					      Matrix4.CreateTranslation(Position);

			return ModelMatrix;
		}
        public Matrix4 GetSetNormalMatrix()
        {
            NormalMatrix = Matrix4.Transpose(Matrix4.Invert(ModelMatrix));
            return NormalMatrix;
        }

		/// <summary>
		/// Reposition the object to co-ordinates specified.
		/// Does not translate the object from a position.
		/// </summary>
		/// <param name="X"></param>
		/// <param name="Y"></param>
		/// <param name="Z"></param>
		public void Reposition(float X, float Y, float Z)
		{
			Reposition(new Vector3(X, Y, Z));
		}
		public void Reposition(Vector3 NewPosition)
		{
            Position = NewPosition;
            GetSetModelMatrix();
            GetSetNormalMatrix();
		}

		/// <summary>
		/// Reset the position of the rotation property and
		/// applies the new one from Vector3.Zero.
		/// </summary>
		/// <param name="X"></param>
		/// <param name="Y"></param>
		/// <param name="Z"></param>
		public void Orientate(float X, float Y, float Z)
		{
			Orientate(new Vector3(X, Y, Z));
		}
		public void Orientate(Vector3 NewRotation)
		{
            Rotation = NewRotation;
            GetSetModelMatrix();
            GetSetNormalMatrix();
		}


		/// <summary>
		/// Adds rotation to an existing orientation, use
		/// negative numbers to rotate anti-clockwise.
		/// </summary>
		/// <param name="X"></param>
		/// <param name="Y"></param>
		/// <param name="Z"></param>
		public void Rotate(float X, float Y, float Z)
		{
			Rotate(new Vector3(X, Y, Z));
		}
		public void Rotate(Vector3 NewRotation)
		{
            Rotation += NewRotation;
            GetSetModelMatrix();
            GetSetNormalMatrix();
		}

		/// <summary>
		/// Adds translation to an existing position, use
		/// negative numbers to translate in the opposite
		/// directional vectors.
		/// </summary>
		/// <param name="X"></param>
		/// <param name="Y"></param>
		/// <param name="Z"></param>
		public void Translate(float X, float Y, float Z)
		{
			Translate(new Vector3(X, Y, Z));
		}
		public void Translate(Vector3 NewTranslation)
		{
			Position += NewTranslation;
            GetSetModelMatrix();
            GetSetNormalMatrix();
		}

		/// <summary>
		/// Scales a mesh in multiple directions, scaling
		/// on one directional vector will lead to stretching.
		/// </summary>
		/// <param name="X"></param>
		/// <param name="Y"></param>
		/// <param name="Z"></param>
		public void Scale(float X, float Y, float Z)
		{
			Scale(new Vector3(X, Y, Z));
		}
		public void Scale(Vector3 NewScale)
		{
			Proportion += NewScale;
            GetSetModelMatrix();
            GetSetNormalMatrix();
		}
        
		public void Update()
		{
			
		}

		public void Draw()
		{   
			// Bind VAO ([0]) and Vertex Position data ([1])
			ObjectMesh.Bind(BufferHandles[0], new uint[] { (int)ShaderUtils.VertexAttributes.VertexPosition });

			// Send model matrix data to shader
			GL.UniformMatrix4((int)ModelMatrixLocation, false, ref ModelMatrix);

			// Draw elements using the indice array from ObjectMesh, with what ever Vertices are buffer bound.
			GL.DrawElements(PrimitiveType.Triangles, ObjectMesh.IndexData.Length, DrawElementsType.UnsignedInt, ObjectMesh.IndexData);

			ObjectMesh.Unbind(new uint[] { (int)ShaderUtils.VertexAttributes.VertexPosition });;
		}
	}
}