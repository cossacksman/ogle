using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Ogle
{
	public class Mesh
	{
		public float[] VertexData { get; private set; }
		public uint[] IndexData { get; private set; }
		public float[] ColorData { get; private set; }
		public float[] TextureData { get; private set; }
		public uint[] Normaldata { get; private set; }

		public int VertexCount { get; private set; }
		public int IndiceCount { get; private set; }
		public int ColorsCount { get; private set; }

		/// <summary>
		/// The constructor does not include any buffer production, this is done afterwards
		/// from the object this class is instanced under, the mesh will be passed as a 
		/// constructor or created through one of many methods available in GameObject.
		/// </summary>
		/// <param name="VertexArray"></param>
		/// <param name="IndexArray"></param>
		/// <param name="ColorArray"></param>
		/// <param name="TextureArray"></param>
		/// <param name="NormalArray"></param>
		public Mesh(float[] VertexArray, uint[] IndexArray, float[] ColorArray = null, float[] TextureArray = null, uint[] NormalArray = null)
		{
			VertexData = VertexArray;
			IndexData = IndexArray;
			ColorData = ColorArray;
			TextureData = TextureArray;
			Normaldata = NormalArray;

			VertexCount = VertexData.Length;
			IndiceCount = IndexData.Length;
			ColorsCount = ColorData.Length;
		}

		/// <summary>
		///  Bind appropriate buffers; Vertex Array Object by handle
		///  and the attribute arrays specified by uint.
		/// </summary>
		/// <param name="VertexArrayHandle"></param>
		/// <param name="VertexAttribArray"></param>
		public void Bind(uint VertexArrayHandle, uint[] VertexAttribArray)
		{
			// Bind VAO by handle
			GL.BindVertexArray(VertexArrayHandle);            
			// Enable attrib arrays
			foreach (uint ArrayID in VertexAttribArray)
				GL.EnableVertexAttribArray(ArrayID);
		}

		/// <summary>
		///  Unbind appropriate buffers; Vertex Array Object
		///  and the attribute arrays specified by uint.
		/// </summary>
		/// <param name="VertexArrayHandle"></param>
		/// <param name="VertexAttribArray"></param>
		public void Unbind(uint[] VertexAttribArray)
		{
			// Unbind VAO
			GL.BindVertexArray(0);
			// Disable attrib array
			foreach (uint ArrayID in VertexAttribArray)
				GL.DisableVertexAttribArray(ArrayID);
			// Unbind VBO 
			GL.BindBuffer(BufferTarget.ArrayBuffer, 0);
		}
	}
}
