using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace Ogle
{
	public static class MeshFactory
	{
		public static Mesh CreateFromFile(string _FileName)
		{
			string FilePath = "Content/Models/" + _FileName + ".obj";

			List<float> _Vertices = new List<float>();
			List<uint> _Indices = new List<uint>();
			List<float> _Colors = new List<float>();
			List<float> _Textures = new List<float>();
			List<uint> _Normals = new List<uint>();

			if (File.Exists(FilePath))
			{
				string _line;

				try
				{
					using (StreamReader _File = new StreamReader(FilePath))
					{
						int i = 0;
						while (!_File.EndOfStream)
						{
							_line = _File.ReadLine();

							// If is vertex data
							#region "V" Vertice data
							if (_line.StartsWith("v "))
							{
								string[] verts = _line.Split(' ');
								_Vertices.Add(float.Parse(verts[1]));
								_Vertices.Add(float.Parse(verts[2]));
								_Vertices.Add(float.Parse(verts[3]));
							}
							#endregion

							// If is a face/texture/uv data
							#region "F" Face data
							else if (_line.StartsWith("f "))
							{
								// If only indice data
								if (_line.Count(c => c == '/') == 0)
								{
									string[] indices = _line.Split(' ');

									// Start at 1 to skip the "f" character
									for (int l = 1; l < indices.Length; l++)
									{
										_Indices.Add(uint.Parse(indices[l]) - 1);
									}
								}
								// If indice and other data
								else if (_line.Count(c => c == '/') > 0)
								{
									string[] rawvertexdata = _line.Split(' ');

									#region INDICE/TEXTURE
									// If there is only one slash (texture exists, normal absent)
									if (rawvertexdata[1].Count(s => s == '/') == 1)
									{
										// Start at 1 to skip the "f" character
										for (int l = 1; l < rawvertexdata.Length; l++)
										{
											int slashpos = rawvertexdata[l].IndexOf('/');

											// Add first number (indice) to local indice list (0 -> slashpos-1)
											_Indices.Add(uint.Parse(rawvertexdata[l].Substring(0, slashpos - 1)));
											// Add remainder after slash to texture [l-1] (slashpos -> (length-slashpos+1))
											_Textures.Add(float.Parse(rawvertexdata[l].Substring(slashpos, (rawvertexdata[l].Length - (slashpos + 1)))));
										}
									}
									#endregion

									#region INDICE//NORMAL
									// If there is a slash next to a slash (normal exists, texture absent)
									else if (rawvertexdata[1][rawvertexdata[1].IndexOf('/') + 1].Equals('/'))
									{
										for (int l = 1; l < rawvertexdata.Length; l++)
										{
											int firstslashpos = rawvertexdata[l].IndexOf('/');
											int lastslashpos = rawvertexdata[l].LastIndexOf('/');

											// Add first number (indice) to global indice list (0 -> slashpos1-1)
											_Indices.Add(uint.Parse(rawvertexdata[l].Substring(0, firstslashpos - 1)));
											// Add second number (normal) to global normal list (slashpos2 -> (length-slashpos2+1))
											_Normals.Add(uint.Parse(rawvertexdata[l].Substring(lastslashpos, rawvertexdata[l].Length - (lastslashpos + 1))));
										}
									}
									#endregion

									#region INDICE/TEXTURE/NORMAL
									// If there are two slashes but they are not next to each other (texture and normal exist)
									else if (rawvertexdata[1].Count(s => s == '/') == 2 && rawvertexdata[1][rawvertexdata[1].IndexOf('/') + 1] != '/')
									{
										List<uint> indices = new List<uint>();
										float[] texture = new float[3];
										List<int> normals = new List<int>();

										for (int l = 1; l < rawvertexdata.Length; l++)
										{
											int firstslash = rawvertexdata[l].IndexOf('/');
											int lastslash = rawvertexdata[l].LastIndexOf('/');

											_Indices.Add(uint.Parse(rawvertexdata[l].Substring(0, firstslash)));
											_Textures[l - 1] = float.Parse(rawvertexdata[l].Substring(firstslash + 1, lastslash - (firstslash + 1)));
											_Normals.Add(uint.Parse(rawvertexdata[l].Substring(lastslash + 1, (rawvertexdata[l].Length - 1) - lastslash)));
										}
									}
									#endregion
								}
							}
							#endregion

							i++;
						}
					}
				}
				catch (Exception ex)
				{
					Console.WriteLine("\n\nException: {0}", ex.Message);
				}

				return new Mesh(_Vertices.ToArray(), _Indices.ToArray(), _Colors.ToArray(), _Textures.ToArray(), _Normals.ToArray());

			}
			else
			{
				throw new FileNotFoundException();
			}
		}
	}
}